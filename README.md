﻿### Installation:
___
* npm/yarn install
* npm/yarn start
___
### For error checking
* Use ESLint on editor
___
### IndiviualData
* Send data (title,image,poster,date,description,etc) as prop
* If you want to render extra component make a function in the CategoryIndiviual Component which returns the extra elements and call this function in IndiviualData.
___
### LinkButton
* Renders a button which links to a url (props.url) and displays text(props.title) in the button.
___
### BackButton
* Renders a button which says back to Category (props.name) and links to Category page URL(props.url)